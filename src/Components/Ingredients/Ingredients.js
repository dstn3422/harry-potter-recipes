import React from 'react';
import styles from './Ingredients.module.css';
import Ingredient from './Ingredient/Ingredient';
import nextId from 'react-id-generator';

const ingredients = (props) => {
  return (
    <div className={styles.Ingredients}>
      <h3>Ingredients</h3>
      <ul className={styles.Ingredients_list}>
        {props.ingredients.map((ingredient) => (
          <Ingredient key={nextId()} ingredient={ingredient} />
        ))}
      </ul>
    </div>
  );
};

export default ingredients;
