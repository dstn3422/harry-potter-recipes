import React from 'react';
import styles from './Ingredient.module.css';
import ingredientImg from '../../../assets/ingredient.jpg';

const ingredient = (props) => {
  return (
    <li className={styles.Ingredient}>
      <img src={ingredientImg} alt='ingredientImg' />
      {props.ingredient}
    </li>
  );
};

export default ingredient;
