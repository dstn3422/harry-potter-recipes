import React from 'react';
import styles from './Potions.module.css';
import Potion from './Potion/Potion';
import nextId from 'react-id-generator';

const potions = (props) => {
  return (
    <div className={styles.Potions}>
      <h3>Potions</h3>
      <ul className={styles.Potions_list}>
        {props.potions.map((potion) => (
          <Potion key={nextId()} name={potion} />
        ))}
      </ul>
    </div>
  );
};

export default potions;
