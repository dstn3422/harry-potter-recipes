import React from 'react';
import styles from './Potion.module.css';
import potionImage from './../../../assets/potion.png';

const potion = (props) => {
  return (
    <li className={styles.Potion}>
      <img className={styles.Potion_image} src={potionImage} alt='Potion' />
      {props.name}
    </li>
  );
};

export default potion;
