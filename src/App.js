import React, { Component } from 'react';
import './App.css';
import Potions from './Components/Potions/Potions';
import Ingredients from './Components/Ingredients/Ingredients';
import CraftingTable from './Components/CraftingTable/CraftingTable';

class App extends Component {
  state = {
    firstClass: {
      potions: [
        'Cure for Boils',
        'Forgetfulness Potion',
        'Herbicide Potion',
        'Sleeping Draught',
        'Wiggenweld Potion',
        'Antidote to Common Poisons',
        'Pompion Potion',
        'Antidote to Common Poisons',
        'Pompion Potion',
        'Antidote to Common Poisons',
        'Pompion Potion',
        'Antidote to Common Poisons',
        'Pompion Potion',
        'Antidote to Common Poisons',
        'Pompion Potion',
        'Antidote to Common Poisons',
        'Pompion Potion',
        'Strength Potion',
      ],
      ingredients: [
        'Infusion of Wormwood',
        'Flobberworm Mucus',
        'Aconite',
        'Asphodel',
        'Dittany',
        'Dragon blood',
        'Moly',
        'Wiggentree bark',
        'Moondew',
        'Salamander blood',
        'Sloth brain mucus',
        'Spine of Lionfish',
        'Fanged Geranium',
        'Snake fangs',
        'Bones',
        'Flitterby Moth',
        'Bouncing Bulb',
        'Foxglove',
      ],
    },
  };
  render() {
    return (
      <div className='App'>
        <CraftingTable />
        <Potions potions={this.state.firstClass.potions} />

        <Ingredients ingredients={this.state.firstClass.ingredients} />
      </div>
    );
  }
}

export default App;
